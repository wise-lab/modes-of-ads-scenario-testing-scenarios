# Creating and Modifying Scenarios

## Install JOSM

A [JOSM](https://josm.openstreetmap.de/) plugin is needed in order to edit scenarios.
Follow the instructions below to get it installed.

1. Install [JOSM](https://josm.openstreetmap.de/).
2. In JOSM, open the configuration dialog window by pressing F12 (or going to **Edit** -> **Preferences**). Click on the **Map Projection Settings** tab on the left, then go to the **Tagging Presets** tab. Add the GeoScenario file `josm/geoscenario_presets_v<version>.xml` to the list of Active Presets by clicking the "plus" button on the upper right. For more information on this step, see ["instructions for configuring map settings"](https://josm.openstreetmap.de/wiki/Help/Preferences/Map).
3. Then, in the **Icon paths** box, click the "plus" button on the bottom right to add the path to the GeoScenario icons (`josm/icons`). This path must be an _absolute_ path (e.g., `/home/<user>/anm_unreal_test_suite/josm/icons`), not a relative path.
4. Go to the **Map Paint Styles** tab and add `josm/geoscenario_style_v<version>.css` to the list of Active Styles in a similar manner to the tagging presets (by pressing the "plus" button in the upper right). Then add the absolute `josm/icons` path to the icon paths by pressing the button in the bottom right.
5. Restart JOSM. The new presets should be visible under the **Presets** menu. Note that you may need to have an active layer in order to open this menu; you can either open an existing document or create a new one via **File** -> **New Layer**.
6. The presets can be added to the toolbar by following the [instructions here](https://josm.openstreetmap.de/wiki/Help/Preferences/Toolbar).

## Modifying Scenarios

### Modifying the `.osm` file

The easiest way to edit this file is using JOSM. This section assumes that the "Install" instructions above have been completed. A full JOSM tutorial is beyond the scope of this document, but additional information can be found at the [JOSM online help](https://josm.openstreetmap.de/wiki/Help).

Each `.osm` file is opened in a separate layer.

We usually consider scenarios in the context of a high-definition map.
The following maps are available:

1. `maps/test_track/firetower.osm`
2. `maps/test_track_cul_de_sac/test_track_cul_de_sac.osm`
3. `maps/test_track_roundabout/test_track_roundabout.osm`

Simply open a map using `File->Open`.

Each scenario specifies which map it requires in the `globalconfig` element,
attribute `lanelet`. For example, traffic jam assist specifies `lanelet=firetower`.
Unfortunately, JOSM cannot open the required map automatically.

The two key elements of a scenario are _nodes_ and _ways_. A node is a single point, and a way is an ordered collection of nodes. In GeoScenario - the format that we use to represent scenarios - nodes include path points, ego and agent spawn locations, goal locations, and scenario triggers, while ways include vehicle and pedestrian paths.

The simplest scenario possible will have only "Ego Start" and "Ego Goal" nodes. More complex scenarios can be created by adding vehicle or pedestrian agents, assigning them to paths, and adding triggers.

Once you are done modifying the file, you can save the layer with `Ctrl` + `S` or by right-clicking the layer and clicking "Save". When closing JOSM, you will see a dialog box warning that there are unsaved changes; this means that the changes have not been uploaded to OSM. You can safely ignore this warning.

Full documentation on the GeoScenario 1.1 format can be found in [GeoScenario_1.1.md](GeoScenario_1.1.md).

Some scenarios only contain a single `.osm` file (e.g., `aaa_traffic_jam_assist.osm`),
some contain a base `.osm` file and multiple `.osm` files, one for each level of difficulty
(e.g., `aaa_cul_de_sac.osm` and `dynamic_objects/aaa_cul_de_sac1.osm` for dynamic interaction level 1).
In the latter case, open the base scenario file and a file for a difficulty level in order to obtain a complete scenario definition.
Layers in JOSM can also be easily merged, if necessary to create a single `.osm` file.
