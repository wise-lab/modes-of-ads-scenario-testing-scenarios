﻿# GeoScenario 1.1

GeoScenario is the format used to represent scenarios.
It is based on the OpenStreetMap format; every GeoScenario document is a valid `.osm` document, using custom properties to define scenario-specific information.

The core components of a GeoScenario file are **nodes** and **ways**. Nodes are single points, and ways are ordered collections of nodes. In GeoScenario, nodes and ways have **GS** tags, which indicate the _type_ of GeoScenario object the node/way is (e.g., `<tag k="gs" v="vehicle"/>` would indicate a _vehicle_ node). Along with the GS tag are other tags assigned to nodes and ways specifying different attributes relevant to the object.

Listed below is a specification of each possible GeoScenario node and way type. The generic format is as follows:

---

### Node/Way Name

**GS**: `<gs_type>` <- The GS type of the object.

A brief description of the object.

A list of the tags that can be applied to the object:
- **Tag 1** (`<tag type> <tag name>`): A brief description of the tag's meaning.
- ...

*Mandatory*: A list of the mandatory tag names needed to specify this GS object.

#### Notes

Further notes on the object.

---

The `<tag type>` is one of the types listed below, and indicates the expected format for the tag value. The `<tag name>` is simply the name of the tag in the GeoScenario XML file.

## Tag Data Types

There are 11 base data types and 1 generic data type expected.

```
int             - An int32, ranging from -2147483648 to 2147483647 (inclusive)
uint            - An unsigned int32, ranging from 0 to 4294967295 (inclusive)
llong           - An int64, ranging from -9223372036854775807 to 9223372036854775807 (inclusive)
ullong          - An unsigned int64, ranging from 0 to 18446744073709551615 (inclusive)
float           - A 4 byte decimal, ranging through +/- 3.4e +/- 38 (~7 digits)
double          - An 8 byte decimal, ranging through +/- 1.7e +/- 308 (~15 digits)
string          - Any input text, does not need to be enclosed with quotes.
bool            - Any of the values {"yes", "on", "true"} for true, and {"no", "off", "false"} for false (case insensitive), unless otherwise specified
version_string  - A string of integers, possibly separated by periods (e.g. "1.1", "2.83.7", etc.)
range           - One of "a..b", "a..", "..b", or "..", where a and b are integers (see below).
vector          - A comma-separated list of three floats with arbitrary whitespace inbetween.
list<T>         - A comma-separated list of values of type T, with arbitrary whitespace inbetween.
```

#### List of Ranges Format

The **Static Reaction** and **Dynamic Interaction** level tags take a "list of ranges" as their value format. A list of ranges is a _comma-separated list of either numbers of "ranges"_.

A range is of the form `a..b`, where `a` and `b` are either the empty-string, or integers.
- `a..b` indicates the levels `a` to `b` (inclusive).
- `..b` indicates the levels 0 to `b`.
- `a..` indicates the levels `a` to 5 (with 5 being the maximum static reaction level).
- `..` indicates levels 0 to 5.

Examples of valid "lists of ranges" include `"4"`, `"1..3"`, `"2.."`, `"0, 3, 5"`, `"1, 4.."`, `"..2,    4..5"`, `"..,1,2,3"`. It doesn't matter if values are repeated.

Examples of invalid lists of ranges include:
- `"6"` - issue: level is too high, levels are clamped between 0 and 5. The GeoScenarioParser will warn that this will be converted to the max value of `5`.
- `"a"` - issue: unable to convert `"a"` to an integer. The GeoScenarioParser will log this as an error, and skip parsing this range.
- `"1.5"` - issue: unable to convert `"1.5"` to an integer, _or_ invalid range format (expected two `.`'s). The GeoScenarioParser will log this as an error, and skip parsing this range.
- `"1...4"` - issue: invalid range format (expected two `.`'s). The GeoScenarioParser will log this as an error, and skip parsing this range.
- `"4..3"` - issue: invalid range format, lower bound is greater than upper bound. The GeoScenarioParser will log this as an error, and skip parsing this range.

If after parsing all ranges, none of them were valid format, the parser will default to assuming _all levels_ (`".."`).

**Note:** `"3..3"` is valid, and will simply be parsed as `3`. `"..0"` and `"5.."` are also valid.

## Nodes

A _node_ is a single LLA point. GeoScenario defines the following presets for nodes:

### Global Config

**GS**: `globalconfig`

Global configurations for a scenario. Defines a name for a scenario, the road network (lanelet), and global failure conditions (timeout and/or collision).

- **Version** (`version_string version`): will indicate the GeoScenario version of the file. For GeoScenario 1.1 (this specification), set this tag value to "1.1".
- **Name** (`string name`): The name given to this scenario.
- **Notes** (`string notes`): Any notes relevant to the scenario (descriptions of the scenario, etc.).
- **Lanelet** (`string lanelet`): Lanelet file which represents the Road Network
- **Collision** (`bool collision`): If `yes` scenario will fail when a collision happens with Ego .., If `no` scenario will not fail when a collision happens with Ego
- **Timeout** (`double timeout`): The amount of seconds that will cause the scenario to fail, if the Ego has not reached it's goal yet within that time
- **Metrics** (`list<string> metric`): Metrics to be collected in this scenario Ex: _*"ttc_ego_v1, ttc_ego_v2"*_.
- **Mutate** (`bool mutate`): _Currently not implemented_
- **Optimize** (`bool optimize`): _Currently not implemented_
- **Optimization Metrics** (`list<string> optmetric`): _Currently not implemented_

*Mandatory:* None

#### Notes

- If **Version** is not specified, then first the parser will attempt to determine the file version based on the _other_ loaded OSM files (see **Multiple OSM File Structure** below). If no version can be deduced from the other files, then the parser will default to version 1.0.
- By default, both Collision and Timeout will be enabled, with Timeout set to 60 seconds.

### Origin point

**GS**: `origin`

Origin point, used as a reference point to translate all coordinates into the simulation coordinate space.

- **Elevation** (`double elevation`): The altitude of the origin, measured in meters above sea level.

*Mandatory:* **Elevation**

### Metric

**GS**: `metric`

Specifies a metric that calculates a value throughout the duration of the scenario simulation.

- **Name** (`string name`): The name given to the metric.
- **Groups** (`list<string> group`): The list of groups this metric is a member of.
- **Reference** (`string reference`): _Currently not implemented_.
- **Agents** (`list<string> agents`): _Currently not implemented_.

*Mandatory*: **Name**, **Reference**, **Agents**

### Location (Node)

**GS**: `location`

Specifies a "location" at the given node. Locations are used to give names to points on the map, for purposes such as spawning/teleporting agents.

- **Name** (`string name`): The name of the location.
- **Groups** (`list<string> group`): The list of groups this location is a member of.

*Mandatory*: **Name**

### Ego Start

**GS**: `egostart`

Indicates the spawn location of the ego. Only one of these should exist per scenario. The ego will be spawned at the location of the first one, with the provided rotation.

- **Name** (`string name`): The name given to the spawn location.
- **Orientation** (`float orientation`): The direction that the ego should be spawned facing, measured in degrees clockwise from East.

*Mandatory:* None

### Ego Goal

**GS**: `egogoal`

Indicates a goal point for the ego. At least one goal must be present in the scenario, otherwise the ego can never pass the scenario (unless there is a _success_ trigger present in the scenario, see Trigger below). Multiple goals can exist per scenario. At runtime, a trigger with radius 2 m will be spawned at the goal point, and will end the scenario as soon as the ego reaches it.

- **Name** (`string name`): The name given to this goal point.
- **Order** (`uint order`): An integer (>= 0) representing the order in which the goal is to be reached by the ego in order to pass the scenario.

*Mandatory:* **Name**

#### Notes

- Goals can be either **ordered** or **unordered**.

##### Ordered Goals

Ordered ego goals are specified by having ego goal nodes with _unique_, nonzero **order** values. This specifies that the ego is expected to reach every goal, in order of the goal's **order** tag from least to greatest.. Ordered goal orders do not have to be contiguous (1, 2, 3, ...), they merely have to be unique and nonzero, so (1, 4, 15, 22, ...) is a valid set of goal orders. If the ego fails to reach all the goals before the **Timeout** (specified in the Global Config), or reaches the goals in the wrong order, then the ego fails the scenario.

If multiple goals have the same nonzero order value, the scenario is deemed _inconsistent_, and will not be valid.

##### Unordered Goals

Unordered ego goals are specified by having _all_ ego goals with **order** set to zero. This specifies that the ego is expected to reach _at least_ one goal. Reaching any unordered goal will pass the scenario.

##### Inconsistent Goal Orders

Any combination of goals with zero and nonzero orders is deemed _inconsistent_, and will not specify a valid scenario.

### Vehicle

**GS**: `vehicle`

Indicates that a scenario vehicle should be spawned at the provided node.

- **Name** (`string name`): The name assigned to this vehicle.
- **Groups** (`list<string> group`): The list of groups this vehicle is a member of.
- **Dynamic Interaction Levels** (`list<range> dynamicinteractionlevels`): The Dynamic Interaction levels at which this vehicle will spawn. The provided value for this field must be a "list of ranges". The format for a list of ranges is indicated below.
- **Orientation** (`float orientation`, previously `rotation`): The direction that the vehicle should spawn facing, measured in degrees clockwise from East.
- **Speed** (`float speed`): The default speed that this vehicle will drive at, in km/h.
- **Use Speed Profile** (`bool usespeedprofile`): `true` if this vehicle should follow the speed profile of its specified path; `false` if it should drive at a constant default speed.
- **Path ID** (`string path`): The name of the path that this vehicle is assigned to.
- **Start In Motion** (`bool start`, previously `startinmotion`): `true` if this vehicle should begin moving immediately when the scenario starts; `false` if it should remain stationary. This property will have no effect unless the vehicle has a path assigned.
- **Cycles** (`int cycles`, previously `cyclelimit`): The maximum number of times that a vehicle will complete its full path. If set to -1, the vehicle will repeat its path forever.

*Mandatory*: **Name**

#### Notes

- If the vehicle's **Path ID** is set to a path with nonzero length, then the vehicle will face in the direction of the tangent at the beginning of the path, overriding the **orientation** tag.

### Pedestrian

**GS**: `pedestrian`

Indicates that a scenario pedestrian should be spawned at the provided node.

- **Name** (`string name`): The name assigned to this pedestrian.
- **Groups** (`list<string> group`): The list of groups this pedestrian is a member of.
- **Dynamic Interaction Levels** (`list<range> dynamicinteractionlevels`): The Dynamic Interaction levels at which this pedestrian will spawn. The provided value for this field must be a "list of ranges". The format for a list of ranges is indicated below.
- **Orientation** (`float orientation`, previously `rotation`): The direction that the pedestrian should spawn facing, measured in degrees clockwise from East.
- **Speed** (`float speed`): The default speed that this pedestrian will drive at, in km/h.
- **Use Speed Profile** (`bool usespeedprofile`): `true` if this pedestrian should follow the speed profile of its specified path; `false` if it should drive at a constant default speed.
- **Path ID** (`string path`): The name of the path that this pedestrian is assigned to.
- **Start In Motion** (`bool start`, previously `startinmotion`): `true` if this pedestrian should begin moving immediately when the scenario starts; `false` if it should remain stationary. This property will have no effect unless the pedestrian has a path assigned.
- **Cycle Limit** (`int cycles`, previously `cyclelimit`): The maximum number of times that a pedestrian will complete its full path. If set to -1, the pedestrian will repeat its path forever.

*Mandatory*: **Name**

#### Notes

- If the pedestrian's **Path ID** is set to a path with nonzero length, then the pedestrian will face in the direction of the tangent at the beginning of the path, overriding the **orientation** tag.

### Static Object (Node)

**GS**: `staticobject`

Indicates that a static object should be spawned at the provided node.

- **Name** (`string name`): The name assigned to this static object.
- **Groups** (`list<string> group`): The list of groups this static object is a member of.
- **Model** (`string model`): The 3D model associated with this static object. (For use in the simulator only).
- **Shape** (`string shape`): The shape mesh used by the object. This must be one of "Cube", "Cone", "Cylinder", "Sphere", or "Plane" (case insensitive).
- **Static Reaction Levels** (`list<range> staticreactionlevels`): The Static Reaction levels at which this static object will spawn. The provided value for this field must be a "list of ranges". The format for a list of ranges is indicated below.
- **Elevation** (`double elevation`): How far off the ground (in centimetres) the object should be placed.
- **Scale X** (`float scalex`): The scale of the object in the x direction (a multiplier).
- **Scale Y** (`float scaley`): The scale of the object in the y direction (a multiplier).
- **Scale Z** (`float scalez`): The scale of the object in the z direction (a multiplier).
- **Scale** (`vector scale`): The scale of the object along the x, y, and z directions. Equivalent to specifying **Scale X**, **Scale Y**, and **Scale Z** individually.
- **Roll** (`float roll`): The rotation of the object along the roll axis, measured in angles.
- **Pitch** (`float pitch`): The rotation of the object along the pitch axis, measured in angles.
- **Yaw** (`float yaw`): The rotation of the object along the yaw axis, measured in angles.
- **Orientation** (`vector orientation`): The orientation of the object specified in euler angles (**Pitch**, **Yaw**, and **Roll**, in that order), measured in angles.
- **Simulate Physics** (`bool simulatephysics`): `"yes"` to indicate that the object should have simulated physics, and `"no"` if not. Static objects with simulated physics have gravity and rotational physics, allowing them to fall, roll, and interact with the environment. Static objects also interact and collide with each other.
- **Height** (`float height`): The object's height in cm.

*Mandatory*: **Name**

#### Notes

- The **model** tag will override the **shape** tag if both are set.

### Trigger

**GS**: `trigger`

Indicates that a trigger should be spawned at the given node. A trigger may have one or more _actions_ assigned to it. Each of these actions will be carried out once, when the specified trigger conditions are met.

- **Name** (`name`): The name assigned to this trigger.
- **Groups** (`list<string> group`): The list of groups this trigger is a member of.
- **Radius** (`float radius`): The radius of the trigger sphere volume (measured in cm).
- **Owner Agent(s)** (`list<string> owner`, previously `actor`): The name(s) of the agent(s) that will activate this trigger when it/they overlaps the trigger volume.
- **Target Agent(s)** (`list<string> target`): The name(s) of the agent(s) that will be affected by the trigger actions.
- **Activation Method** (`string activate`): The method of activation of the trigger. This must be one of "location", "time", or "condition" (case insensitive).
- **Time** (`double time`): If the trigger is a _time trigger_, then **Time** specifies the amount of time after which the trigger is activated.
- **Metric** (`string condition`): If the trigger is a _condition trigger_, then **Metric** specifies a metric which, upon reaching the given **Value**, activates the trigger.
- **Value** (`double value`): The value which must be reached by the given **Metric** in order to activate a _condition trigger_.
- **Delay** (`delay`): The delay in seconds between the agent overlapping the trigger and the action being executed.
- **Action Start** (`astart`, previously `actionstart`): If this tag is present, the target agent will start moving (if a path is assigned) upon the trigger's activation.
- **Action Pass** (`asuccess`): If this tag is present, the ego will pass the scenario upon the trigger's activation.
- **Action Fail** (`afail`): If this tag is present, the ego will fail the scenario upon the trigger's activation.
- **Action Change Speed** (`float aspeed`, previously `actionspeed`): If this tag is present, the target agent will change to this speed in km/h upon the trigger's activation.
- **Action Change `UseSpeedProfile`** (`bool aspeedprofile`): Sets the target agent(s)'s `UseSpeedProfile` tag to the provided value upon the trigger's activation.
- **Action Follow Path** (`string apath`, previously `actionpath`): If this tag is present, the specified path will be assigned to the target agent(s) upon the trigger's activation.
- **Action Change State** (`string astate`): _Currently not implemented_

*Mandatory*: **Name**, **Owner Agent(s)**

#### Notes

##### Location Trigger

_Location Triggers_ are the most traditional form of trigger, and are activated simply when one of the owner agents collides with the trigger area. Location triggers are the default trigger type if the **Activation Method** tag is missing, or doesn't match one of "location", "time", or "condition".

##### Time Trigger

_Time Triggers_ are activated when the given **Time** tag is reached after the start of the scenario. Note that the presence of the **Time** tag does not override the value of the **Activation Method** tag, meaning a **Time** tag on a _Location Trigger_ has no meaning.

##### Condition Trigger

_Condition Triggers_ are activated only when the given **Metric** tag achieves the value specified by the **Value** tag. Note that the presence of the **Metric**/**Value** tag does not override the value of the **Activation Method** tag. Additionally, if the **Value** tag is not specified, it is assumed to be zero.

### Path node

**GS**: None

The use of path nodes is explained in the next section "Ways"

- **Target speed of agent (km/h)** (`double agentspeed`) - the velocity that the agent should have when it reaches this path node.
- **Target acceleration of agent (g)** (`double agentacceleration`) - the acceleration the agent should use to achieve the velocity at the next node.
- **Time to reach target acceleration of agent (s)** (`double timetoacceleration`) - the time in which an agent should reach the given acceleration. The acceleration is linearly increased from the agent's previous acceleration to the target one over this period of time.

*Mandatory*: **Target Speed of Agent (km/h)**

#### Notes

- If `timetoacceleration` is specified, but `agentacceleration` is not, then `timetoacceleration` is ignored.
- The `agentacceleration` tag specifies the target acceleration in units of `g` (9.81 m/s) for the agent at that node, and this target acceleration will be reached in a duration of time after it hits this node, specified by the `timetoacceleration` tag, in units of seconds.
- The acceleration will change linearly for this entire duration, but if it hits the next node before it reaches this target acceleration, it will move on to the next target.
- If the `agentacceleration` tag is not specified, then the agent speed is linearly interpolated (with respect to _displacement_ by default) between the previous path node `agentspeed` and the next node `agentspeed`.

### Traffic Light

**GS**: `trafficlight`

_Currently not implemented_.

## Ways

A _way_ is an ordered collection of nodes, typically denoting a path, route, or boundary. A way can be either "open" or "closed". A _closed_ way is a way in which the first and last node are connected with an edge (or more specifically, the first and last node ref in the GS XML file are _identical_). Closed ways are visually represented by the following diagram.

![](https://lh6.googleusercontent.com/SBa_3psJf1CU7Hq1s6iTwKBTFZDMZABhnLQ4Zsh0WIW5W4KmmI-IC5TrRf-a4fVAGGRhtp8oSS_bMQ1QMeuzkOGtFGk5m7vrcvwEcToH2KLusQYgLt_a8-ODi-fgRKFyIiezL0bE)

Closed ways are still represented by `way` elements in the GS XML file, they are distinguished from ordinary ways simply by their topology.

In GeoScenario 1.1, there are multiple presets for ways.

### Path

**GS**: `path`

Indicates that this way represents a path to be followed by a vehicle or pedestrian. A closed way indicates that the path is meant to be looped.

- **Name** (`string name`): The name assigned do this path.
- **Groups** (`list<string> group`): The groups this path is a member of.
- **Virtual** (`bool virtual`): Whether or not this path is "virtual" (see below).

#### Notes

By default, a scenario agent will travel along a path at a constant speed. In order to specify a speed profile for an agent, that agent must have the **Use Speed Profile** (`usespeedprofile`) tag set to `true`, and be assigned to a path where every node has is a _Path node_ (see above). The agent will always try to match the speed of the next node in its path, but it will only try to match the acceleration of the node that it has most recently passed. Agent speed profiling is also _adaptive_, in that if it is unable to reach the desired speed of the node by the time it is reached, then it will use the node's acceleration parameters (`agentacceleration` and `timetoacceleration`) along with the _current_ speed to attempt to reach the _next_ node desired speed.

If an agent is assigned to a path, and the agent has `cycles` set to a value other than 1 or 0, then:
- If the path is closed, the agent will smoothly traverse the looped path until the desired number of cycles is reached.
- If the path is open, the agent will teleport to the beginning of the path until the desired number of cycles is reached, upon which the agent will remain fixed at the _end_ of the path.

##### Virtual Paths

When an agent is assigned to a path (either at spawn time, or with a trigger), by default the agent is teleported to the first node in the path. However, if the path is _virtual_, then the _path_ is teleported so that the first node of the path overlaps the location of the agent assigned to said path. This is what makes a path "virtual": it is able to be moved around and used as a template for agents, regardless of where the agent is in the map.

### Static Object (Way)

**GS**: `staticobject`

An _open_ way specifies a _wall_ static object, creating a polyline wall mesh with edges/vertices given by the way.

A _closed_ way specifies a static object mesh with the shape of a polygonal-based prism. The base of the prism is specified by the edges/vertices of the way.

_Currently not implemented_.

### Location (Way)

**GS**: `location`

An _open_ way specifies a polyline location, and a _closed_ way specifies an area location. Each of these location types specifies a _named region_ of the map that can be referenced for purposes like spawning objects. Spawning an object at an open way location allows the object to spawn anywhere along the vertices/edges of the way, and spawning an object at a closed way location allows the object to spawn anywhere within the _area_ of the way.

- **Name** (`string name`): The name given to this location.
- **Groups** (`list<string> group`): The groups this location is a member of.
- **Continuous** (`bool continuous`): (Only applicable to _open_ way locations) Whether or not objects are allowed to spawn along the entire way (including the edges) (true), or only at the vertices (false). An open way location with `continuous` set to false is called _discrete_.

## The Road Network
Lanelets are used to represent the scenario's Road Network. The Road Network is stored in a separate XML file to make replacements easy. However, a scenario can only be interpreted within the context of the Road Network. Thus, a Geoscenario must always be distributed with its associated Road Network file. The Lanelet file is defined inside the Global Config element.

![](images/lanelet_example.png)

## Difficulty Levels

The **Static Reaction** and **Dynamic Interaction** levels mentioned above are **difficulty levels**. Scenarios can be launched with different difficulty levels, corresponding to how "hard" the scenario is for the autonomous vehicle to navigate. Difficulty levels can be specifed along multiple _axes_, corresponding to different _types_ of difficulty.

The Static Reaction level corresponds to the difficulty of _static_ objects. This includes
- where static obstacles are placed in the scenario (are there none, are they on the road, are they in intersections, etc.)
- where is there occlusion in the scenario (can the autonomous vehicle see everything, are there blind spots, etc.)

The Dynamic Interaction level corresponds to the difficulty of _dynamic_ objects. This includes
- the difficulty levels of vehicles (how many vehicles are there, what paths do they take, how do they behave on the road, etc.)
- the difficulty levels of pedestrians (how many pedestrians are there, do they follow paths that get in the way of the road, etc.)

When you run a scenario, you have to supply levels of difficulty to each of `static reaction` and `dynamic interaction`.

### Creating Scenarios With Different Difficulty Levels

When you create a GeoScenario file, you can supply different difficulty level tags to the different objects. These are the `staticreactionlevels` and `dynamicinteractionlevels` tags mentioned above. By specifying different difficulty levels for the objects, you can create arbitrarily complex scenarios.

Since there are 6 levels of static reaction and dynamic interaction, allowing for 36 possible difficulty configurations in total, it is often reasonable to
- A) assume either the static reaction levels or the dynamic interaction levels are capped at some low number like 2. A common option is to limit the static reaction level to either 0, 1, or 2, where 0 corresponds to "nothing", 1 corresponds to "occlusion", and 2 corresponds to "obstacles on the road and occlusion".
- B) try to create scenarios where the difficulty levels do not conflict. For example, it would be difficult to maintain a scenario where dynamic interaction level 3 and static reaction level 2 includes an intersection with an obstacle in the middle that an oncoming car swerves around, because you would have to ensure that the car's swerving path doesn't interfere with the desired behaviour of your scenario for _other_ static reaction levels. (i.e. would it make sense to run the scenario with static reaction level 1 and dynamic interaction level 3?)

Additionally, maintaining 36 different difficulty configurations at once can become cumbersome in JOSM. To mitigate this complexity, it is possible to split the difficulty levels into separate `osm` files. This functionality is described in the **Multiple OSM File Structure** section.

### Multiple OSM File Structure

A scenario can be split up into multiple OSM files corresponding to the different dynamic interaction levels and static reaction levels. The folder structure **must** be as follows:

```
<scenario_name>/
    dynamic_objects/
        <scenario_name>0.osm       <-- Dynamic Interaction Level 0
        ...
        <scenario_name>5.osm       <-- Dynamic Interaction Level 5
    static_objects/
        <scenario_name>0.osm       <-- Static Reaction Level 0
        ...
        <scenario_name>5.osm       <-- Static Reaction Level 5
    <scenario_name>.json
    <scenario_name>.osm            <-- Base OSM file
    scenario_config.bash
    scenario_route.gpx
```

The names of the `osm` files in the `dynamic_objects` and `static_objects` folders _must_ be suffixed by an integer from `0` to `5` (inclusive) indicating the difficulty level the `osm` file corresponds to.

The Base OSM file includes information that is common to the scenario regardless of the difficulty. The `dynamic_objects/scenarioX.osm` file indicates _solely_ the information that is to be spawned at dynamic interaction level X. Similarly, the `static_objects/scenarioY.osm` file indicates _solely_ the information that is to be spawned at a static reaction level Y.

You can create scenarios in this format by running `bash scripts/create_scenario.bash` with the `-s` option (or the `--split` option).
You can launch scenarios in this format by running `bash scripts/launch.bash`. The Scenario Manager is intelligent enough to determine if the simulation should launch with the multiple .osm file structure.

When you run `bash scripts/launch.bash <scenario> X Y`, the GeoScenarioParser will load the contents of
- `scenario/scenario.osm`
- `scenario/dynamic_objects/scenarioX.osm`
- `scenario/static_objects/scenarioY.osm`
as if they were in a single file to run in the simulation.

**Note:** The `dynamicinteractionlevels` and `staticreactionlevels` tags on `vehicle`, `pedestrian`, and `staticobject` nodes are _optional_ in the `osm` files `dynamic_objects/scenarioX.osm` and `static_objects/scenarioY.osm`. If, however, they are included and they _contradict_ the inferred difficulty level from the file name (e.g., a `staticobject` node with `staticreactionlevels == 1` is in `static_objects/scenario4.osm`), it will _not_ be loaded when the simulator is run with a static reaction level of 4, regardless of the fact that they are in the appropriate difficulty `osm` file.
