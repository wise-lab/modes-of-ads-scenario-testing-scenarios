<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Scenarios from paper "Modes of Automated Driving System Scenario Testing: Experience Report and Recommendations"</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://git.uwaterloo.ca/wise-lab/modes-of-ads-scenario-testing-scenarios/" property="cc:attributionName" rel="cc:attributionURL">Waterloo Intelligent Systems Engineering (WISE) Lab </a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

# Scenarios from paper "Modes of Automated Driving System Scenario Testing: Experience Report and Recommendations"

The repository contains the six scenario-based tests from the paper

[Antkiewicz et al., "Modes of Automated Driving System Scenario Testing: Experience Report and Recommendations", SAE WCX, 2020](https://uwaterloo.ca/waterloo-intelligent-systems-engineering-lab/publications/modes-automated-driving-system-scenario-testing-experience).

## GeoScenario

The choreography of the tests is expressed using GeoScenario 1.1 format.
For more information, see [docs/GeoScenario_1.1.md](docs/GeoScenario_1.1.md) or
an online reference [GeoScenario on ReadTheDocs](https://geoscenario.readthedocs.io/en/latest/).

## WISE Sim

The videos below show the executions of the scenarios in [WISE Sim](http://uwaterloo.ca/waterloo-intelligent-systems-engineering-lab/projects/wise-sim), which is a simulator developed at [WISE Lab](http://uwaterloo.ca/waterloo-intelligent-systems-engineering-lab).
WISE Sim implements a scenario runner for GeoScenario 1.1, which loads the scenario definitions, populates the simulated environment, and executes the logic of the scenario (moves the agents, handles triggers, etc.).

## Viewing the scenarios

Each scenario comes with a `.png` image containing the rendering of the scenario,
one image per level of difficulty.

1. Traffic jam assist
([Simulation video](http://wiselab.uwaterloo.ca/wise-sim/wise-sim-traffic_jam_assist-simulation.webm))

<img src="scenarios/aaa_traffic_jam_assist/aaa_traffic_jam_assist.png" alt="Rendering of traffic jam assist" width="70%">

The POV drives in a stop-and-go style following a strict velocity and acceleration profile.
The EGO follows while maintaining safe distance.

2. Cul-de-sac with regular traffic
([Simulation interaction level 1 video](http://wiselab.uwaterloo.ca/wise-sim/wise-sim-cul_de_sac-level1-simulation.webm))

<img src="scenarios/aaa_cul_de_sac/aaa_cul_de_sac_level_1.png" alt="Rendering of cul-de-sac with regular traffic" width="40%">

The POV drives around the cul-de-sac and the EGO merges into the traffic.

3. Left turn at stop with occlusion
([Simulation video](http://wiselab.uwaterloo.ca/wise-sim/wise-sim-left_turn_at_stop_with_occlusion-simulation.webm))
([Mixed reality video](http://wiselab.uwaterloo.ca/wise-sim/wise-sim-left_turn_at_stop_with_occlusion-mixed-reality.mp4))

<img src="scenarios/aaa_left_turn_at_stop_with_occlusion/aaa_left_turn_at_stop_with_occlusion.png" alt="Rendering of left turn at stop with occlusion" width="25%">

The POV emerges from behind the occlusion 5s after EGO stopped at the stop line.
The EGO begins to drive after stopping but has to yield again after the POV emerges.
The EGO continues the left turn after the POV has passed.

4. Roundabout with road debris
([Simulation video](http://wiselab.uwaterloo.ca/wise-sim/wise-sim-roundabout_with_road_debris-simulation.webm))

<img src="scenarios/aaa_roundabout_with_road_debris/aaa_roundabout_with_road_debris.png" alt="Rendering of roudabout with road debris" width="50%">

The EGO turns left a a roundabout and encounters a 0.33x0.33x0.33 cm road debris (qube).

5. Straight with pedestrian
([Simulation interaction level 0 video](http://wiselab.uwaterloo.ca/wise-sim/wise-sim-straight_with_pedestrian-level0-simulation.webm))
([Simulation interaction level 1 video](http://wiselab.uwaterloo.ca/wise-sim/wise-sim-straight_with_pedestrian-level1-simulation.webm))

<img src="scenarios/aaa_straight_with_pedestrian/aaa_straight_with_pedestrian.png" alt="Rendering of straight with pedestrian" width="35%">

The EGO drives straight and encounters a pedestrian crossing the road.

* Level 0: the EGO has enough time to stop and yield to the pedestrian.
* Level 1: the EGO does not have enough time to stop and crashes into the pedestrian.

6. Right turn at stop with pedestrian
([Simulation interaction level 1 video](http://wiselab.uwaterloo.ca/wise-sim/wise-sim-right_turn_with_pedestrian_level1-simulation.webm))

<img src="scenarios/aaa_right_turn_with_pedestrian/aaa_right_turn_with_pedestrian.png" alt="Rendering of right turn at stop with pedestrian" width="30%">

The EGO turns right at a stop and yields to a pedestrian crossing the road.

## Editing the scenarios

The scenarios can be edited using JOSM.
For more information, see [docs/Creating_scenarios.md](docs/Creating_scenarios.md).
